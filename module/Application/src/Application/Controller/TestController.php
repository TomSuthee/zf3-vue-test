<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Db\ResultSet\ResultSet;
use Application\Models\Users;
use Zend\Json\Json;
use Zend\View\Model\JsonModel;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\Adapter\Filesystem;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;
use end\Cache\Storage\Plugin\ExceptionHandler;

class TestController extends AbstractActionController
{
    private $cache = null;

    public function __construct()
    {
        $this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);

        // cache
        $this->cache = new \Zend\Cache\Storage\Adapter\Filesystem();
        $this->cache->getOptions()->setTtl(3600);

        $plugin = new \Zend\Cache\Storage\Plugin\ExceptionHandler();
        $plugin->getOptions()->setThrowExceptions(false);
        $this->cache->addPlugin($plugin);
    }

    public function indexAction()
    {
        $view = new ViewModel();
        $view->setTemplate('test/index');
        return $view;
    }

    public function question1Action()
    {
        $parameter = $this->params()->fromRoute('parameter', '');
        $result = '';

        // trim, remove 'x' and split into array
        $parameter = str_replace(' ', '', $parameter);
        $parameter = str_ireplace('x', '', $parameter);
        $numbers = explode(',', $parameter);
        $numbers = array_filter($numbers, function ($number) {
          return $number !== '';
        });

        // looking for cache
        $cacheKey = 'question1-' . implode('-', $numbers);
        $result = $this->lookupForCache($cacheKey, function () use ($numbers) {
          $tmp = $this->getNextSequential($numbers);
          if ($tmp === null) {
              $tmp = 'Invalid';
          }
          return $tmp;
        });

        $this->response->getHeaders()->addHeaderLine('Content-Type: text/plain');
        return $this->response->setContent($result);
    }

    public function question2Action()
    {
        $a = intval(str_replace(' ', '', $this->params()->fromRoute('a', '')));
        $b = intval(str_replace(' ', '', $this->params()->fromRoute('b', '')));
        $c = intval(str_replace(' ', '', $this->params()->fromRoute('c', '')));
        $d = intval(str_replace(' ', '', $this->params()->fromRoute('d', '')));

        // looking for cache
        $cacheKey = 'question2-' . implode('-', [$a, $b, $c, $d]);
        $result = $this->lookupForCache($cacheKey, function () use ($a, $b, $c, $d) {
          return $d - ($b * $c) - $a;
        });

        $this->response->getHeaders()->addHeaderLine('Content-Type: text/plain');
        return $this->response->setContent($result);
    }

    public function question3Action()
    {
      $parameter = $this->params()->fromRoute('parameter', '');
      $result = '';

      $parameter = str_replace(' ', '', $parameter);
      $pairs = explode(',', $parameter);

      $numbers = [];
      $lastIndex = 0;
      $invalid = false;
      foreach ($pairs as $pair) {
        $pair = explode('=', $pair);
        if (intval($pair[0]) != $lastIndex + 1) {
          $invalid = true;
          break;
        }

        $lastIndex++;

        if (stripos($pair[1], 'x') === false && $pair[1] != '') {
          $numbers[] = $pair[1];
        }
      }

      if ($invalid) {
        $result = 'Invalid';
      } else {
        // looking for cache
        $cacheKey = 'question3-' . implode('-', $numbers);
        $result = $this->lookupForCache($cacheKey, function () use ($numbers) {
          $tmp = $this->getNextMultiply($numbers);
          if ($tmp === null) {
              $tmp = 'Invalid';
          }
          return $tmp;
        });
      }

      $this->response->getHeaders()->addHeaderLine('Content-Type: text/plain');
      return $this->response->setContent($result);
    }

    private function getNextSequential(Array $numbers, $level = 0)
    {
        if ($level === 0) {
            // prepare calculation variables
            $numbers = [$numbers];
            $result = $this->getNextSequential($numbers, $level + 1);
            if ($result === null) {
                return null;
            }
            return end($numbers[$level]) + $result;
        } else {
            $nextLevel = [];
            $numbersToProcess = $numbers[$level - 1];
            // if there's only 1 number left, we cannot process anymore, return null
            if (count($numbersToProcess) == 1) {
                return null;
            }

            // get the differential between each number
            for ($index = 0; $index < count($numbersToProcess) - 1; $index++) {
                $nextLevel[] = $numbersToProcess[$index + 1] - $numbersToProcess[$index];
            }

            if (count(array_unique($nextLevel)) == 1) {
                // if all the array elements are the same value, we reach the top differential level
                // return the differential value
                return end($nextLevel);
            } else {
                if (count($nextLevel) == 2) {
                    // if there're 2 element left and they're not the same, we cannot process anmore
                    return null;
                } else {
                    // calculate the next $level
                    $numbers[] = $nextLevel;
                    $result = $this->getNextSequential($numbers, $level + 1);
                    if ($result === null) {
                        return null;
                    }
                    return end($numbers[$level]) + $result;
                }
            }
        }
    }

    private function getNextMultiply(Array $numbers)
    {
        // Verify value
        $totalElement = count($numbers);
        for ($index = 1; $index < $totalElement; $index++) {
          $thisValue = (($index + 1) * pow(10, $index)) + $numbers[$index - 1];
          if ($thisValue != $numbers[$index]) {
            return null;
          }
        }

        // calculate the next value
        return (($totalElement + 1) * pow(10, $totalElement)) + end($numbers);
    }

    private function lookupForCache($key, $fnc)
    {
      if ($this->cache->hasItem($key)) {
        return $this->cache->getItem($key) . ' (from cache)';
      } else {
        $value = $fnc();
        $this->cache->setItem($key, $value);
        return $value  . ' (from calculation)';
      }
    }
}
