function init()
{
  var question1 = new Vue({
    el: '#app-question-1',
    data: {
      sequence: '3, 5, 9, 15, X',
      result: '',
      processing: false
    },
    methods: {
      calculate: function () {
        var _this = this
        _this.processing = true;
        axios.get('/question1/' + _this.sequence)
          .then(function (response) {
            _this.result = response.data;
            _this.processing = false;
        });
      }
    }
  });

  var question2 = new Vue({
    el: '#app-question-2',
    data: {
      a: 24,
      b: 10,
      c: 2,
      d: 99,
      result: '',
      processing: false
    },
    methods: {
      calculate: function () {
        var _this = this
        _this.processing = true;
        axios.get('/question2/' + _this.a + '/' + _this.b + '/' + _this.c + '/' + _this.d)
          .then(function (response) {
            _this.result = response.data;
            _this.processing = false;
        });
      }
    }
  });

  var question3 = new Vue({
    el: '#app-question-3',
    data: {
      sequence: '1 = 5 , 2 = 25 , 3 = 325 , 4 = 4325 , 5 = X',
      result: '',
      processing: false
    },
    methods: {
      calculate: function () {
        var _this = this
        _this.processing = true;
        axios.get('/question3/' + _this.sequence)
          .then(function (response) {
            _this.result = response.data;
            _this.processing = false;
        });
      }
    }
  });
}

function doNothing(){}
